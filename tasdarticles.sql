-- phpMyAdmin SQL Dump
-- version 4.1.6
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 08, 2018 at 12:29 AM
-- Server version: 5.6.16
-- PHP Version: 5.5.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `tasdarticles`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
  `IDcategorie` int(11) NOT NULL AUTO_INCREMENT,
  `nomCategorie` varchar(50) NOT NULL,
  `descriptionCategorie` varchar(300) NOT NULL,
  PRIMARY KEY (`IDcategorie`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `clients`
--

CREATE TABLE IF NOT EXISTS `clients` (
  `IDclient` varchar(50) NOT NULL,
  `nomClient` varchar(50) NOT NULL,
  `prenomClient` varchar(50) NOT NULL,
  `dateNaissance` date NOT NULL,
  `photoClient` varchar(50) NOT NULL,
  PRIMARY KEY (`IDclient`),
  KEY `IDclient` (`IDclient`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `employes`
--

CREATE TABLE IF NOT EXISTS `employes` (
  `IDemployes` int(11) NOT NULL AUTO_INCREMENT,
  `nomEmploye` varchar(50) NOT NULL,
  `prenomEmploye` varchar(50) NOT NULL,
  `dateNaissance` date NOT NULL,
  `photoEmploye` varchar(50) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(20) NOT NULL,
  `status` varchar(20) NOT NULL DEFAULT 'Employe',
  PRIMARY KEY (`IDemployes`),
  KEY `IDemployes` (`IDemployes`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `inscriptions`
--

CREATE TABLE IF NOT EXISTS `inscriptions` (
  `IDinscription` int(11) NOT NULL AUTO_INCREMENT,
  `IDinscrit` varchar(50) NOT NULL,
  `dateInscription` int(11) NOT NULL,
  `IDauteur` int(11) NOT NULL,
  PRIMARY KEY (`IDinscription`),
  KEY `IDauteur` (`IDauteur`),
  KEY `IDinscrit` (`IDinscrit`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `locations`
--

CREATE TABLE IF NOT EXISTS `locations` (
  `IDlocation` int(11) NOT NULL AUTO_INCREMENT,
  `dateLocation` date NOT NULL,
  `dateRetour` date NOT NULL,
  `IDemprunteur` int(11) NOT NULL,
  `IDloueur` int(11) NOT NULL,
  `IDmateriel` int(11) NOT NULL,
  `quantiteLouee` int(11) NOT NULL,
  PRIMARY KEY (`IDlocation`),
  KEY `IDmateriel` (`IDmateriel`),
  KEY `IDloueur` (`IDloueur`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `materiels`
--

CREATE TABLE IF NOT EXISTS `materiels` (
  `IDmateriel` int(11) NOT NULL AUTO_INCREMENT,
  `nomMateriel` varchar(50) NOT NULL,
  `caracteristique` varchar(300) NOT NULL,
  `prixMembre` double NOT NULL,
  `prixRegulier` double NOT NULL,
  `photoMateriel` varchar(50) NOT NULL,
  `disponibilite` int(11) NOT NULL,
  `referenceIDcategorie` int(11) NOT NULL,
  PRIMARY KEY (`IDmateriel`),
  KEY `IDmateriel` (`IDmateriel`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `membres`
--

CREATE TABLE IF NOT EXISTS `membres` (
  `codePermanent` varchar(50) NOT NULL,
  `nomMembre` varchar(50) NOT NULL,
  `prenomMembre` varchar(50) NOT NULL,
  `dateNaissance` date NOT NULL,
  `photoMembre` varchar(50) NOT NULL,
  PRIMARY KEY (`codePermanent`),
  KEY `codePermanent` (`codePermanent`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ventes`
--

CREATE TABLE IF NOT EXISTS `ventes` (
  `IDvente` int(11) NOT NULL AUTO_INCREMENT,
  `dateVente` date NOT NULL,
  `prixVente` double NOT NULL,
  `IDvendeur` int(11) NOT NULL,
  `IDacheteur` varchar(50) NOT NULL,
  `IDmateriel` int(11) NOT NULL,
  `quantiteVendue` int(11) NOT NULL,
  PRIMARY KEY (`IDvente`),
  KEY `IDmateriel` (`IDmateriel`),
  KEY `IDacheteur` (`IDacheteur`),
  KEY `IDmateriel_2` (`IDmateriel`),
  KEY `IDacheteur_2` (`IDacheteur`),
  KEY `IDvendeur` (`IDvendeur`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `inscriptions`
--
ALTER TABLE `inscriptions`
  ADD CONSTRAINT `inscriptions_ibfk_2` FOREIGN KEY (`IDinscrit`) REFERENCES `membres` (`codePermanent`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `inscriptions_ibfk_1` FOREIGN KEY (`IDauteur`) REFERENCES `employes` (`IDemployes`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `locations`
--
ALTER TABLE `locations`
  ADD CONSTRAINT `locations_ibfk_2` FOREIGN KEY (`IDloueur`) REFERENCES `employes` (`IDemployes`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `locations_ibfk_1` FOREIGN KEY (`IDmateriel`) REFERENCES `materiels` (`IDmateriel`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `materiels`
--
ALTER TABLE `materiels`
  ADD CONSTRAINT `materiels_ibfk_1` FOREIGN KEY (`IDmateriel`) REFERENCES `ventes` (`IDmateriel`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `membres`
--
ALTER TABLE `membres`
  ADD CONSTRAINT `membres_ibfk_1` FOREIGN KEY (`codePermanent`) REFERENCES `ventes` (`IDacheteur`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `ventes`
--
ALTER TABLE `ventes`
  ADD CONSTRAINT `ventes_ibfk_2` FOREIGN KEY (`IDacheteur`) REFERENCES `clients` (`IDclient`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `ventes_ibfk_1` FOREIGN KEY (`IDvendeur`) REFERENCES `employes` (`IDemployes`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
